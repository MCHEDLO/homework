package com.example.homework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()


    }

    private fun init(){
        val RandomButton = findViewById<Button>(R.id.RandomButton)
        val RandomnumberTextview = findViewById<TextView>(R.id.RandomnumberTextview)
        val yesno = findViewById<TextView>(R.id.yesno)
        RandomButton.setOnClickListener {
            val number:Int = randomNumber()
            d("Random number","THIS IS RANDOM NUMBER $number")
            RandomnumberTextview.text = number.toString()
            yesno.text = dividedbyfive(number)
        }
    }

    private fun randomNumber() = (-100..100).random()

    fun dividedbyfive(randomNumber:Int):String {
        if (randomNumber%5==0 && randomNumber/5>0){
            return "Yes"
        }else{
            return "NO"
        }

    }





}


